# Pandoc/LaTeX & Unicode

TLDR: The default LaTeX PDF engine (`pdflatex`) is not very Unicode aware. It
doesn't know how to layout Unicode characters, but there is usually a LaTeX
command for the character you can use instead.

For example, if you need a $\Sigma$, you can use `$\Sigma$` instead of the
unicode character.

---

The `md2pdf` alias:

```sh
md2pdf () {
  # usage: md2pdf the-file.md --other-args --etc
  pandoc -s "$1" -o "${1%%.md}.pdf" -V geometry:margin=1in \
  --citeproc \                # pandoc >= 2.11 (Arch, Fedora 35)
  --filter=pandoc-citeproc \  # older versions, e.g. Ubuntu / Debian
  ${@:2}  # any extra args (after the filename)
}
```
