---
header-includes:
  - \DeclareUnicodeCharacter{03A3}{\Sigma}
---

# Character Redefinition

However, if you know the LaTeX command for the symbol but want to use the
Unicode character anyway, you can redefine the unicode character code (`U+THIS`)
to the corresponding LaTeX command in the metadata header:

For example:

![](error.png)

```yml
header-includes:
  - \DeclareUnicodeCharacter{03A3}{\Sigma}
```

Now if you need a $\Sigma$, you can use `$\Sigma$` or $Σ$.

---

This uses regular ol' `md2pdf` with no changes:

```sh
md2pdf 2-declare.md
```
