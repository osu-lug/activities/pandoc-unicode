---
# mainfont: NotoSerif-Regular.ttf
---

# Other PDF engines

Additionally, you can use a different PDF engine e.g. `xelatex` or `lualatex`
that will handle unicode characters correctly without the LaTeX command.

For example, if you need a $\Sigma$, you can use `$\Sigma$` or Σ, font
permitting -- note that the default LaTeX font (Latin Modern) does not cover the
full Unicode, but XeLaTeX will pick up the unicode version if you have it (e.g.
`otf-cm-unicode` package in the AUR), or you can specify a manual font that
does. XeLaTeX will show a warning if the font does not have the symbol.

Generally, XeLaTeX is drop-in for PDFLaTeX, but some packages might have
incompatibility, though since this is in Pandoc you're likely not doing anything
too complex and it shouldn't be a problem.

---

This does need an extra arg -- either add it to md2pdf or specify it manually
(`pdf-engine` can't be specified in the metadata header):

```sh
md2pdf 3-xelatex.md --pdf-engine=xelatex
```
